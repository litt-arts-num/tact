# **/ ! \ Ce dépôt n'est plus vraiment maintenu/utilisé. Le code est maintenant [ici](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/tact), sur l'instance gitlab de l'UGA.**


## TACT - plateforme de Transcription et d'Annotation de Corpus Textuels

## A propos

TACT est un logiciel principalement développé au sein de l'[UMR Litt&Arts](https://litt-arts.univ-grenoble-alpes.fr/) (Université Grenoble Alpes, CNRS) qui s'appuie principalement sur [Symfony 4](https://symfony.com/4).

Il offre notamment la possibilité d'héberger différents projets de transcription et d’annotation de sources numérisées, chacun pouvant avoir ses spécificités (communauté plus ou moins ouverte, simple transcription ou annotation selon un schéma, nombre de relectures nécessaires, etc.). Cela permet de déléguer la transcription à des non-spécialistes, tout en contrôlant la qualité de l’exécution, et ce via une interface simple d’utilisation.

<details>
  <summary>Un joli schéma qui résume pluôt bien le tout.</summary>
  <img src="./misc/tact.png">
</details>

## Languages & bibliothèques logicielles utilisées

-   PHP (Symfony 4)
-   Javascript
-   OpenSeaDragon
-   Fontawesome 5
-   intro.js
-   TinyMCE
-   Bootstrap 4

## Documentation

### Installation

voir le [wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/tact/-/wikis/Installation)

### Utilisation

Des manuels d'utilisation sont disponibles sur la plateforme :

-   [Manuel contributeur](https://tact.demarre-shs.fr/platform/manuel_contributeur.pdf)
-   [Manuel gestionnaire](https://tact.demarre-shs.fr/platform/manuel_gestionnaire.pdf)

Une ébauche de documentation est disponible sur le
[wiki](https://gitlab.com/litt-arts-num/tact/-/wikis/home)

## Contributions

**Toute contribution est bienvenue.**

Voir le [Gitlab graph](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/tact/-/graphs/master-new) et [le fichier AUTHORS](AUTHORS.md)

<details>
  <summary>A toute fin utile, un schéma "rapide" concernant la base de données</summary>
  <img src="./misc/db.png">
</details>


## Merci :heart:

-   Julien Fagot (CDD UMR Litt&Arts) pour son [application](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/outils-tact/schema-generator) de création et gestion de schéma
-   Myriam EL HELOU & Sami BOUHOUCHE (Master 2 IDL UGA) pour leur [script Python](https://github.com/elheloum/TEI2JSON) de création de schema. Une version (plus vraiment) maintenue est disponible [ici](https://gitlab.com/litt-arts-num/tei2json).

(Manifestez vous si on vous a oublié...)

## Licence

GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))
