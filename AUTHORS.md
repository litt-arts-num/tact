# Contribution au code
* Arnaud Bey (UMR Litt&Arts, Université Grenoble Alpes)
* Patrick Guillou (Démarre SHS !)
* Sylvain Hatier (Démarre SHS !)
* Anne Garcia-Fernandez (UMR Litt&Arts, CNRS)
* Alexander Grebenkov (MSH Lorraine)

# Aide pour les spécifications, test de la plateforme, documentation, animation
* Célia Marion (Démarre SHS !)
* Elisabeth Greslou (UMR Litt&Arts, Université Grenoble)
* Isabelle Pignone (MSH Lorraine)

# Contributions diverses
* Brigitte Combe (UMR Litt&Arts, Université Grenoble)
* Cécile Meynard (CIRPaLL, Université d'Angers)
