# Principaux changements

> 01/2025

* La définition de la langue dans les propriétés avancées d'un projet est requise pour que la bouton concernant Tesseract s'affiche lors de la transcription. 
* Amélioration du système de sélection du gestionnaire de projet (placeholder pour forcer sélection & affichage nom et prénom)

> 11/2024

* Intégration de l'éditeur de schéma d'encodage

> 10/2024

* Ajout de raccourcis vers certains éléments de l'administration d'un projet depuis le menu "Administrer"
* Ajout d'un lien, à l'accueil d'un projet, incitant les gestionnaires de projet à déposer des médias s'il n'y en a pas.
* Les fichiers markdown peuvent être importés en tant que transcriptions.
* Dans l'interface de transcription, l'image n'est plus affichée quand il s'agit de l'image par défaut (dans le cas où on fait juste de la correction d'OCR par exemple et qu'il n'y a pas de média associé à la transcription). On gagne ainsi de la place pour l'éditeur de texte.
* Quelques précautions ont été prises à l'import des médias (via .zip), évitant ainsi que des fichiers non image se retrouvent chargés. 

> 09/2024

* Des paramètres de plateforme ont été ajoutés : 
  * logo de plateforme
  * image de la page d'accueil
  * couleur principale
* Affichage des dernières contributions sur la page des projets
* Ajout de la prise en charge de l'attribut "pattern" permettant de contrainre les valeurs d'attribut à une regexp 

> 08/2024

* Fusion des fonctionnalités "palamède" (sharedocs, audio/video, PDF)
* Les logs des transcriptions sont visibles par les transcripteurs
* Ajout d'une propriété "préfix" aux serveurs (IIIF) permettant la construction d'URL pratique pour Omeka C. 
* Ajout d'une fonctionnalité de message global à la plateforme, s'affichant partout (pour prévenir d'une maintenance par exemple)

> 05/2024

* Le balises custom sont gérées par la CSS de TinyMCE

> 02/2023

* Les gestionnaires de projet peuvent inscrire des utilisateurs par lot à leur projet.
* Le statut des transcriptions est visible depuis les logs.
* Nettoyage de code

> 01/2023

* Ajout de la licence GNU GPL 3 (à la place de la CC pas vraiment adaptée...)

> 12/2022

* Les commentaires liés aux transcriptions sont aussi listés sur la page d'accueil des projets.

> 04/2022

* Les gestionnaires de projet peuvent recevoir des mails lors des inscriptions.
* Les gestionnaires de projet et relecteurs peuvent recevoir des messages internes lors des demandes de relecture.
* Les inscriptions non validées aux projet sont mises en avant.

> 03/2022

* A l'export, on peut spécifier un dossier pour ne pas avoir à tout exporter.
* On peut choisir de n'exporter seulement les transcriptions validées.
* Ajout d'une page avec quelques statistiques sommaires.
* Ajout de warnings pour éviter de perdre une transcription/relecture non sauvegardée.

> 01/2022

* On stocke maintenant le diff entre deux version d'un transcription dans les logs.
* Dans les transcriptions, les balises sont "namespacées" pour éviter des conflits avec les balises HTML (title, form, etc.).

> 12/2021

* Développement & mise en ligne d'un outil de génération de schéma.

> 10/2021

* ajout d'une propriété au schéma JSON pour gérer l'utilisation des balises à la racine de la transcription
* possibilité d'aller voir le média suivant ou précédent pendant la transcription

> 08/2021

* les news de TACT sont gérées sur un blog Hypothèses et on y fait référence ici via le flux RSS.

> 06/2021

* Ajout de Tesseract

> 03/2021

* Regroupement des fonctionnalités d'administration d'un projet

> 10/2020

* possibilité de s'affranchir de l'arborescence d'un projet et d'afficher tous les médias.
* interface plus responsive
* les projets sont maintenant triés par dernière activité.
* affichage de l'avancement des transcriptions par dossier
* possibilité de s'envoyer des messages entre utilisateurs
* possibilité pour un gestionnaire de projet d'envoyer un message à tous les inscrits d'un projet

> 09/2020

* possibilité de faire référence à des médias hébergés sur des serveurs type IIIF
* installation plus facile de TACT

> 02/2020

* application possible d'une XSLT à l'export d'un projet
* export de métadonnées liées à la plateforme/projet

> 12/2019

* possibilité de notification par mail des événements pour les gestionnaires de projet (en plus des notifications en interne)
* suivi de l'activité via une page de logs, par projet ou plus globalement sur la plateforme.

> 08/2019

* possibilité d'importer des transcriptions pré-existantes


# v1.11.0

> 08/02/2019

### Main changes
##### New features
* Reviews
* Project "statistics"
* Contributors are credited
* Log display
* Users can comment transcription
* Project are exportable
* Internal message system
* Admin & project manager can manually (un)validate transcriptions
* Transcription can be reported
* Projects no longer need transcription scheme
* "My Contribution" & full-text filter on transcription list
* Tutorial

##### Bug fixes & minor changes
* Update Symfony to 4.3
* Update tinymce to 5.0 (chrome bug solved)
* Media list display
* Translations
* ELAN logo
* Makefile cmd order
* Dead code removed (clean repositories, etc.)
* Disable openseadragon shortcuts
* Performance related changes.
* Add this changelog :)
* etc.


##### Full git log
```
git log v1.0.0...v1.11.0 --date=short --pretty=format:'* %ad - %s - https://gitlab.com/litt-arts-num/tact/commit/%H'
```


# v1.0.0
> 10/01/2019

First version
