<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlatformRepository")
 */
class Platform
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tesseractUrl;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $questionnaireUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mailAdress;

    /**
     * @ORM\Column(type="boolean")
     */
    private $displayNameNavBar = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $homePicture;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getTesseractUrl(): ?string
    {
        return $this->tesseractUrl;
    }

    public function setTesseractUrl(?string $tesseractUrl): self
    {
        $this->tesseractUrl = $tesseractUrl;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getQuestionnaireUrl(): ?string
    {
        return $this->questionnaireUrl;
    }

    public function setQuestionnaireUrl(?string $questionnaireUrl): self
    {
        $this->questionnaireUrl = $questionnaireUrl;

        return $this;
    }

    public function getMailAdress(): ?string
    {
        return $this->mailAdress;
    }

    public function setMailAdress(?string $mailAdress): self
    {
        $this->mailAdress = $mailAdress;

        return $this;
    }

    public function getDisplayNameNavBar(): ?bool
    {
        return $this->displayNameNavBar;
    }

    public function setDisplayNameNavBar(bool $displayNameNavBar): self
    {
        $this->displayNameNavBar = $displayNameNavBar;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getHomePicture(): ?string
    {
        return $this->homePicture;
    }

    public function setHomePicture(?string $homePicture): self
    {
        $this->homePicture = $homePicture;

        return $this;
    }
}
