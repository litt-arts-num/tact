<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 */
class Media
{
    const TYPE_IMAGE = 'image';
    const TYPE_PDF = 'pdf';
    const TYPE_AUDIO = 'audio';
    const TYPE_VIDEO = 'video';

    const VIDEO_TYPE_YOUTUBE = 'youtube';
    const VIDEO_TYPE_PEERTUBE = 'peertube';
    const VIDEO_TYPE_CANAL_U = 'canal-u';
    const VIDEO_TYPE_VIMEO = 'vimeo';
    const VIDEO_TYPE_LOCAL = 'local';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="medias")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $thumbnailUrl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $videoType;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Transcription", inversedBy="media", cascade={"persist", "remove"})
     */
    private $transcription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Directory", inversedBy="medias")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IiifServer", inversedBy="medias")
     */
    private $iiifServer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaServer", inversedBy="medias")
     */
    private $mediaServer;

    /**
     * @ORM\OneToMany(targetEntity=MetadataMedia::class, mappedBy="media", orphanRemoval=true)
     */
    private $metadatas;

    public static function isPathPdf(string $path): bool
    {
        return pathinfo(strtolower($path), PATHINFO_EXTENSION) === 'pdf';
    }

    public static function isPathAudio(string $path): bool
    {
        return in_array(pathinfo(strtolower($path), PATHINFO_EXTENSION), ['mp3', 'wav', 'ogg']);
    }

    public static function isPathVideo(string $path): bool
    {
        return in_array(pathinfo(strtolower($path), PATHINFO_EXTENSION), ['mp4', 'avi', 'mkv', 'mov', 'flv', 'wmv']);
    }

    public static function getAvailableVideoTypes(): array
    {
        return [
            self::VIDEO_TYPE_YOUTUBE,
            self::VIDEO_TYPE_VIMEO,
            self::VIDEO_TYPE_PEERTUBE,
            self::VIDEO_TYPE_CANAL_U,
            self::VIDEO_TYPE_LOCAL,
        ];
    }

    public static function getPathType(?string $path): string
    {
        if (!$path) {
            return self::TYPE_IMAGE;
        }

        if (static::isPathPdf($path)) {
            return static::TYPE_PDF;
        }

        if (static::isPathAudio($path)) {
            return static::TYPE_AUDIO;
        }

        if (static::isPathVideo($path)) {
            return static::TYPE_VIDEO;
        }

        return self::TYPE_IMAGE;
    }

    public function __construct()
    {
        $this->metadatas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParent(): ?Directory
    {
        return $this->parent;
    }

    public function setParent(?Directory $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getTranscription(): ?Transcription
    {
        return $this->transcription;
    }

    public function setTranscription(?Transcription $transcription): self
    {
        $this->transcription = $transcription;

        return $this;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function isCloudVideo(): bool
    {
        return $this->isVideo() && in_array(
                $this->getVideoType(),
                [
                    self::VIDEO_TYPE_YOUTUBE,
                    self::VIDEO_TYPE_VIMEO,
                    self::VIDEO_TYPE_PEERTUBE,
                    self::VIDEO_TYPE_CANAL_U,
                ]
            );
    }

    public function isCloudUrl(): bool
    {
        if ($this->isCloudVideo()) {
            return true;
        }

        $url = $this->getUrl();
        if (!$url) {
            return false;
        }

        return strpos($url, 'http://') === 0 || strpos($url, 'https://') === 0;
    }

    public function isNeedToProxy(): bool
    {
        if (!$this->isCloudUrl()) {
            return false;
        }

        return !!$this->getMediaServer();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIiifServer(): ?IiifServer
    {
        return $this->iiifServer;
    }

    public function setIiifServer(?IiifServer $iiifServer): self
    {
        $this->iiifServer = $iiifServer;

        return $this;
    }

    /**
     * @return MediaServer|null
     */
    public function getMediaServer(): ?MediaServer
    {
        return $this->mediaServer;
    }

    /**
     * @param MediaServer|null $mediaServer
     */
    public function setMediaServer(?MediaServer $mediaServer = null): self
    {
        $this->mediaServer = $mediaServer;

        return $this;
    }

    /**
     * @return Collection|MetadataMedia[]
     */
    public function getMetadatas(): Collection
    {
        return $this->metadatas;
    }

    public function addMetadata(MetadataMedia $metadata): self
    {
        if (!$this->metadatas->contains($metadata)) {
            $this->metadatas[] = $metadata;
            $metadata->setMedia($this);
        }

        return $this;
    }

    public function removeMetadata(MetadataMedia $metadata): self
    {
        if ($this->metadatas->contains($metadata)) {
            $this->metadatas->removeElement($metadata);
            // set the owning side to null (unless already changed)
            if ($metadata->getMedia() === $this) {
                $metadata->setMedia(null);
            }
        }

        return $this;
    }

    public function getThumbnailUrl(): ?string
    {
        if ($this->thumbnailUrl) {
            return $this->thumbnailUrl;
        }

        if ($this->isAudio() || $this->isCloudUrl()) {
            return null;
        }

        $url = $this->getUrl();
        if (!$url) {
            return $url;
        }

        if (!$this->isPdf()) {
            return $url;
        }

        $info = pathinfo($url);

        return ($info['dirname'] ? $info['dirname'] . DIRECTORY_SEPARATOR : '')
            . $info['filename']
            . '.'
            . 'jpg';
    }

    public function setThumbnailUrl(?string $thumbnailUrl = null)
    {
        $this->thumbnailUrl = $thumbnailUrl;

        return $this;
    }

    public function setType(string $type = null): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): string
    {
        if ($this->type) {
            return $this->type;
        }

        return static::getPathType($this->getUrl());
    }

    public function getVideoType(): ?string
    {
        return $this->videoType;
    }

    public function setVideoType(?string $videoType): self
    {
        if (!!$videoType && !in_array($videoType, self::getAvailableVideoTypes())) {
            throw new \InvalidArgumentException(sprintf('Unknown video type "%s"', $videoType));
        }
        $this->videoType = $videoType;

        return $this;
    }

    public function isAudio(): bool
    {
        return $this->type === static::TYPE_AUDIO || $this->isMatchType(static::TYPE_AUDIO);
    }

    public function isPdf(): bool
    {
        return $this->type === static::TYPE_PDF || $this->isMatchType(static::TYPE_PDF);
    }

    public function isImage(): bool
    {
        return $this->getType() === static::TYPE_IMAGE;
    }

    public function isVideo(): bool
    {
        return $this->getType() === static::TYPE_VIDEO;
    }

    public function isRecognizable(): bool
    {
        return $this->isImage();
    }

    private function isMatchType(string $type): bool
    {
        $url = $this->getUrl();
        if (!$url) {
            return false;
        }

        return static::getPathType($url) === $type;
    }
}
