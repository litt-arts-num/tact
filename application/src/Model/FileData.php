<?php

namespace App\Model;

class FileData
{
    private $fileName;

    private $type;

    public function __construct(string $fileName, string $type)
    {
        $this->fileName = $fileName;
        $this->type = $type;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
