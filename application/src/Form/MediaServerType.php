<?php

namespace App\Form;

use App\Entity\MediaServer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediaServerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name', TextType::class, [
            'label' => 'media_server_server_name',
            'required' => true,
          ])
          ->add('url', TextType::class, [
            'label' => 'media_server_server_url',
            'required' => true,
          ])
          ->add('mediaXpath', TextType::class, [
            'label' => 'media_server_media_xpath',
            'required' => false,
          ])
          ->add('save', SubmitType::class, array(
              'label' => 'send',
              'attr' => [
                'class' => 'btn btn-primary'
              ]
          ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => MediaServer::class,
        ));
    }
}
