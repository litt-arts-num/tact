<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\UserStatus;

class InviteProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('status', EntityType::class, [
            'class' => UserStatus::class,
            'label' => 'status',
            'translation_domain' => 'messages',
            'choice_label' => 'name',
            'choice_translation_domain' => 'messages',
            'help' => 'Le statut qui sera assigné aux utilisateurs invités.'
          ])
          ->add('users', TextareaType::class, [
            'label' => 'users',
            'help' => 'Un par ligne. Renseigner le nom d\'utilisateur ou l\'adresse mail. Doit déjà avoir un compte sur TACT.',
            'required' => true,
          ])
          ->add('save', SubmitType::class, [
            'label' => 'message_send',
            'attr' => [
              'class' => 'btn btn-primary'
            ]
          ]);
    }
}
