<?php

namespace App\Twig;

use Jfcherng\Diff\Factory\RendererFactory;
use Jfcherng\Diff\Renderer\RendererConstant;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class DiffExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('diff2html', array($this, 'diff2html')),
        );
    }

    public function diff2html($diff)
    {
        $rendererOptions = [
          'detailLevel' => 'word',
          'language' => 'eng',
          'lineNumbers' => false,
          'separateBlock' => true,
          'showHeader' => true,
          'spacesToNbsp' => false,
          'tabSize' => 4,
          'mergeThreshold' => 0.8,
          'cliColorization' => RendererConstant::CLI_COLOR_ENABLE,
          'outputTagAsString' => false,
          'jsonEncodeFlags' => \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE,
          'wordGlues' => [' ', '-'],
          'resultForIdenticals' => null,
          'wrapperClasses' => ['diff-wrapper'],
        ];

        $htmlRenderer = RendererFactory::make('SideBySide', $rendererOptions);
        $html = $htmlRenderer->renderArray(json_decode($diff, true));

        return $html;
    }
}
