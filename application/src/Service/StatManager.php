<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class StatManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getUsersCountByRange()
    {
      return [
        $this->em->getRepository('App:User')->getCountByRange('last day'),
        $this->em->getRepository('App:User')->getCountByRange('last week'),
        $this->em->getRepository('App:User')->getCountByRange('last month'),
        $this->em->getRepository('App:User')->getCountByRange('last year'),
      ];
    }

    public function getSignupCountByYear()
    {
        $from = $this->getFirstSignupYear();
        $to = date('Y');
        $signupCount = [];
        $cumul = 0;
        while ($from <= $to) {
          $signupCount[$from] = $cumul + $this->em->getRepository('App:User')->getSignupCountByYear($from);
          $cumul = $signupCount[$from];
          $from++;
        }

        return $signupCount;
    }

    public function getSignupTotal()
    {
        return $this->em->getRepository('App:User')->countAll();
    }

    public function getFirstSignupYear()
    {
        $firstUser = $this->em->getRepository('App:User')->findOneBy([], ['createdAt' => 'ASC'], 1);
        $date = $firstUser->getCreatedAt();

        return $date->format('Y');
    }

}
