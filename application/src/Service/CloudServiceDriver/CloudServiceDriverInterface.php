<?php

namespace App\Service\CloudServiceDriver;

use App\Entity\MediaServer;

interface CloudServiceDriverInterface
{
    public function supports(string $requestUrl, MediaServer $server): bool;

    public function getThumbnail(string $fileUrl, string $requestUrl, MediaServer $server): ?string;

    public function getFileName(string $fileUrl, string $requestUrl, MediaServer $server): string;

}
