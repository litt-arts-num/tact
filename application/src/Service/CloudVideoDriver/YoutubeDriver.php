<?php

namespace App\Service\CloudVideoDriver;

use App\Entity\Media;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class YoutubeDriver extends AbstractVideoDriver
{
    private const URL = 'https://www.youtube.com/oembed';

    private $httpClient;

    /**
     * @param $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getType(): string
    {
        return Media::VIDEO_TYPE_YOUTUBE;
    }

    public function getVideoData(string $videoId): VideoInfo
    {
        $url = 'https://www.youtube.com/watch?v=' . $videoId;
        $response = $this->httpClient->request(
            Request::METHOD_GET,
            self::URL . '?' . http_build_query([
                'url' => $url,
                'format' => 'json',
            ])
        );

        $responseData = $response->toArray();

        return new VideoInfo(
            $url,
            $this->prepareVideoFileName($responseData['title']),
            $responseData['thumbnail_url']
        );
    }
}
