<?php

namespace App\Service\CloudVideoDriver;

use App\Entity\Media;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CanalUDriver extends AbstractVideoDriver
{
    private const HOST = 'https://www.canal-u.tv';

    private $httpClient;

    /**
     * @param $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getType(): string
    {
        return Media::VIDEO_TYPE_CANAL_U;
    }

    public function getVideoData(string $videoId): VideoInfo
    {
        $url = self::HOST . '/embed/' . $videoId;
        $response = $this->httpClient->request(Request::METHOD_GET, $url);

        $crawler = new Crawler($response->getContent());

        $titleCrawler = $crawler->filter('head>title')->first();

        $title = $titleCrawler ? trim(str_replace('| Canal U', '', $titleCrawler->text())) : null;

        $previewImage = $crawler->filter('video')->first()->attr('poster');

        return new VideoInfo($url, $this->prepareVideoFileName($title), $previewImage);
    }
}
