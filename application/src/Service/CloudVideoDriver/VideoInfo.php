<?php

namespace App\Service\CloudVideoDriver;

class VideoInfo
{
    private $videoUrl;

    private $thumbnailUrl;

    private $title;

    public function __construct(string $videoUrl, ?string $title, ?string $thumbnailUrl)
    {
        $this->videoUrl = $videoUrl;
        $this->title = $title;
        $this->thumbnailUrl = $thumbnailUrl;
    }

    public function getVideoUrl(): string
    {
        return $this->videoUrl;
    }

    public function getThumbnailUrl(): ?string
    {
        return $this->thumbnailUrl;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function isValid(): bool
    {
        return !!$this->videoUrl && !!$this->title && !!$this->thumbnailUrl;
    }
}
