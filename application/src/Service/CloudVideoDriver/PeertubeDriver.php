<?php

namespace App\Service\CloudVideoDriver;

use App\Entity\Media;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PeertubeDriver extends AbstractVideoDriver
{
    private const HOST = 'https://peertube.fr';

    private $httpClient;

    /**
     * @param $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getType(): string
    {
        return Media::VIDEO_TYPE_PEERTUBE;
    }

    public function getVideoData(string $videoId): VideoInfo
    {
        $url = self::HOST . '/api/v1/videos/' . $videoId;
        $response = $this->httpClient->request(Request::METHOD_GET, $url);

        $responseData = $response->toArray();

        return new VideoInfo(
            $url,
            $this->prepareVideoFileName($responseData['name']),
            self::HOST . $responseData['previewPath']
        );
    }
}
