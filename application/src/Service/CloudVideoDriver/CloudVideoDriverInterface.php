<?php

namespace App\Service\CloudVideoDriver;

interface CloudVideoDriverInterface
{
    public function getType(): string;

    public function getVideoData(string $videoId): VideoInfo;
}
