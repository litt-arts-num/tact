<?php

namespace App\Service;

use App\Entity\Media;
use App\Exception\ServiceException;
use App\Repository\PlatformRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TextRecognitionService
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var PlatformRepository
     */
    private $platformRepository;

    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        PlatformRepository $platformRepository,
        HttpClientInterface $httpClient,
        MediaManager $mediaManager,
        LoggerInterface $logger
    ) {
        $this->httpClient = $httpClient;
        $this->platformRepository = $platformRepository;
        $this->mediaManager = $mediaManager;
        $this->logger = $logger;
    }

    public function recognizeMedia(Media $media, string $appUrl): string
    {
        $result = "";
        $appUrl = ($media->getIiifServer()) ? "" : $appUrl;

        if (!$media->isRecognizable()) {
            throw new ServiceException('Media is not recognizable');
        }

        $platform = $this->platformRepository->getPlatformParameters();
        $baseUrl = $platform->getTesseractUrl();

        if (!$baseUrl) {
            return $result;
        }

        $project = $media->getProject();

        $tesseractLanguage = $project->getTesseractLanguage();
        $languagesSuffix = "";
        if ($tesseractLanguage) {
            $languages = explode(" ", $tesseractLanguage);
            foreach ($languages as $language) {
                $languagesSuffix .= "&lang=".$language;
            }
        }
        $imgUrl = $media->isCloudUrl() ? $media->getUrl() : $appUrl.trim($this->mediaManager->generateURL($media));

        $url = $baseUrl.'?img='.$imgUrl.$languagesSuffix;

        $response = $this->httpClient
            ->request(
                Request::METHOD_GET,
                $url,
                [
                    'headers' => [
                        'Content-type' => 'application/json',
                    ],
                    'timeout' => 100000,
                ]
            );

        try {
            $result = json_decode($response->getContent());
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable);
        }

        return $result;
    }
}
