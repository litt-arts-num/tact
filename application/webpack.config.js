const Encore = require('@symfony/webpack-encore')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin-legacy');
const path = require('path')

Encore
  // the project directory where compiled assets will be stored
  .setOutputPath('public/build/')
  // the public path used by the web server to access the previous directory
  .setPublicPath('/build')
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())

  // define the assets of the project
  .addEntry('js/app', './assets/js/app.js')
  .addEntry('js/edit-advanced-project', './assets/js/edit-advanced-project.js')
  .addEntry('js/schema', './assets/js/schema.js')
  .addEntry('js/edit-basic-project', './assets/js/edit-basic-project.js')
  .addEntry('js/user-status-form', './assets/js/user-status-form.js')
  .addEntry('js/user-list', './assets/js/admin/user-list.js')
  .addEntry('js/account', './assets/js/account.js')
  .addEntry('js/project-media', './assets/js/project-media.js')
  .addEntry('js/submit-disable', './assets/js/submit-disable.js')
  .addEntry('js/project-export', './assets/js/project-export.js')
  .addEntry('js/transcription', './assets/js/transcription.js')
  .addEntry('js/platform', './assets/js/platform.js')
  .addEntry('js/editorial-content', './assets/js/editorial-content.js')
  .addEntry('js/openseadragon', './node_modules/openseadragon/build/openseadragon/openseadragon.min.js')
  .addEntry('js/vue', './node_modules/vue/dist/vue.esm-bundler.js')
  .addEntry('js/file-input', './node_modules/bs-custom-file-input/dist/bs-custom-file-input.min.js')
  .addEntry('js/json-editor', './assets/js/json-editor.js')

  .addPlugin(new CopyWebpackPlugin([
    // Copy the skins from tinymce to the build/js/skins directory
    {
      from: 'node_modules/tinymce/skins',
      to: 'js/skins'
    },
  ]))

  .addStyleEntry('css/app', './assets/css/global.scss')
  .addStyleEntry('css/json-editor', './assets/css/json-editor.css')
  .addStyleEntry('css/account', './assets/css/account.scss')
  .addStyleEntry('css/create-project', './assets/css/create-project.scss')
  .addStyleEntry('css/project-media', './assets/css/project-media.scss')
  .addStyleEntry('css/user', './assets/css/admin/user.scss')
  .addStyleEntry('css/toastr', './node_modules/toastr/build/toastr.min.css')
  .addStyleEntry('css/intro', './node_modules/intro.js/minified/introjs.min.css')

  .enableSassLoader(function () {}, {
    resolveUrlLoader: false
  })

  .addLoader({
    test: /\.svelte$/,
    loader: 'svelte-loader',
  })
  .addLoader({
    test: /\.js$/,
    loader: 'babel-loader',
    exclude: /node_modules\/(?!svelte\-forms).+/,
    query: {
      presets: ['stage-3', ['env', {useBuiltIns: 'usage', corejs: 3}]],
      plugins: [['transform-runtime', {
        'polyfill': false,
        'regenerator': true
      }]]
    },
  })
  // will prefix css properties according to the supported browser set in postcss.config.js
  .enablePostCssLoader()

  .configureBabel(function (babelConfig) {
    babelConfig.presets = ['stage-3', ['env', {useBuiltIns: 'usage', corejs: 3}]];
    babelConfig.plugins.push(['transform-runtime', {
      "polyfill": false,
      "regenerator": true
    }]);
    //
    //babelConfig.presets.push('stage-3', ['env', {useBuiltIns: 'usage', corejs: 3}])
  }, {
    includeNodeModules: ['svelte-forms'],
  })
  // .configureBabelPresetEnv((config) => {
  //   config.useBuiltIns = "usage";
  //   config.corejs = 3;
  // })

  .createSharedEntry('vendor', [
    'jquery',
    'bootstrap',
    '@fortawesome/fontawesome-free/js/all'
  ])

  // for legacy applications that require $/jQuery as a global variable
  .autoProvidejQuery()
  .autoProvideVariables({
    'Routing': 'router',
    'Toastr': 'Toastr',
    'Translator': 'Translator',
    'TinyEditor': 'TinyEditor',
    'TeiEditor': 'TeiEditor'
  })
  .enableVersioning()

if (Encore.isProduction()) {
  Encore.addPlugin(new TerserPlugin(
      {
      },
  ))
}

// get "REAL" webpack object
const config = Encore.getWebpackConfig()
config.resolve.alias = {
  'router': path.resolve(__dirname, 'modules/router.js'),
  'Toastr': path.resolve(__dirname, 'modules/toastr.js'),
  'Translator': path.resolve(__dirname, 'modules/translator.js'),
  'TinyEditor': path.resolve(__dirname, 'modules/tiny-editor.js'),
  'TeiEditor': path.resolve(__dirname, 'modules/tei-editor.js'),
}

//UglifyJsPlugin does not supports es6 syntax, so we use terser
config.plugins = config.plugins.filter((plugin) => plugin.constructor.name !== 'UglifyJsPlugin');

// https://stackoverflow.com/questions/44439909/confusion-over-various-webpack-shimming-approaches
config.module = Object.assign(config.module, {
  loaders: [{
    test: require.resolve('tinymce/tinymce'),
    loaders: [
      'imports?this=>window',
      'exports?tinymce'
    ]
  },
  {
    test: /tinymce\/(themes|plugins)\//,
    loaders: [
      'imports?this=>window'
    ]
  }
  ]
})

module.exports = config
