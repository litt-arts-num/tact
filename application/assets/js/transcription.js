/* global mode */
import introJs from 'intro.js'
import MediaViewer from './media-viewer';

// normally every 2 minutes but to avoid any time problem let's put a bit less
const updateLogTimeout = 100000
const viewerClass = '.media-viewer';
const viewer = document.querySelector(viewerClass)
const mediaUrl = viewer.getAttribute('data-url').trim()
const mediaType = viewer.getAttribute('data-type').trim()
const mediaOptions = JSON.parse(viewer.getAttribute('data-options').trim())

let homeBtn = document.querySelector('#media-navigator-home')
let nextBtn = document.querySelector('#media-navigator-next')
let previousBtn = document.querySelector('#media-navigator-previous')

let warningDiv = document.querySelector('#warning-media-changed')
let mainSaveBtn = document.querySelector('#main-save-btn')

const mediaViewer = new MediaViewer({
  selectors: {
    content: '.media-viewer-content',
    imageViewerId: 'seadragon-viewer',
    imageViewerPanel: '.openseadragon-buttons',
    pdfViewer: '.pdf-viewer',
    audioPlayer: '.audio-player',
    videoPlayer: '.video-player-transcription',
  },
})

let editor
mediaViewer.show(mediaType, mediaUrl, mediaOptions)

$(document).ready(() => {

  $('#media-navigator-next').on('click', (e) => {
    let btn = e.target
    btn.disabled = true
    displayOtherFacsimile(btn, true)
  })

  $('#media-navigator-previous').on('click', (e) => {
    let btn = e.target
    btn.disabled = true
    displayOtherFacsimile(btn, false)
  })

  let homeButton = $('#media-navigator-home');
  homeButton.on('click', (e) => {
    displayOriginalFacsimile(e.target)
  }).attr('disabled', true)


  $('.send-report').on('click', (e) => {
    reportTranscription(e.target.dataset.id)
  })

  mediaViewer.onShow(({type, url}) => {
    homeButton.attr('disabled', url === mediaUrl);
  })

  if ('edit' === mode) {
    lockTranscription()
    preventLeaving()
    testFirstTranscript()
    const jsonTeiDef = JSON.parse(document.getElementById('tei-schema').value)
    editor = new TeiEditor(jsonTeiDef)
    editor.init()

    $('.btn-validation-modal').on('click', () => {
      $('#validation-modal').modal('show')
    })

    $('.btn-tesseract').on('click', (e) => {
      tesseract(e.target.dataset.id, e.target.dataset.url, e.target)
    })

    $('.btn-save-transcription').on('click', (e) => {
      saveTranscription(e.target.dataset.id, e.target)
    })

    $('#start-tutorial').on('click', () => {
      startTutorial()
    })


  } else if ('validation' === mode) {
    lockTranscription()
    preventLeaving()
    const jsonTeiDef = JSON.parse(document.getElementById('tei-schema').value)
    editor = new TeiEditor(jsonTeiDef)
    editor.init()

    $('.btn-save-transcription').on('click', (e) => {
      saveTranscription(e.target.dataset.id, e.target)
    })

  } else {
    $('.img-fluid').on('click', (e) => {
      const image = e.target.cloneNode()
      image.classList.remove('project-image')
      image.setAttribute('style', 'width:100%;')
      const modalBody = $('.project-media-modal').find('.modal-body')
      modalBody.empty()
      modalBody.append(image)
      $('.project-media-modal').modal('show')
    })
  }

  $('#start-tutorial').on('click', () => {
    startTutorial()
  })
})

const lockTranscription = () => {
  const logIdEl = document.getElementById('log-id')
  if (logIdEl) {
    const logId = logIdEl.value
    window.setInterval(() => {
      updateLockedLog(logId)
    }, updateLogTimeout)
  }
}

const updateLockedLog = (id) => {
  const url = Routing.generate('transcription_log_locked_update', {
    id: id
  })
  $.ajax({
    method: 'POST',
    url: url
  }).done(() => {})
}

const tesseract = (id, imgURL, btn) => {
  $('#tesseract-modal').modal('show')

  const url = Routing.generate('media_tesseract', {
    id: id
  })

  $.ajax({
    method: 'POST',
    url: url,
    data: {
      'imgURL': imgURL
    }
  }).done((data) => {
    data = data.replace(/(?:\r\n|\r|\n)/g, '<br>')
    editor.setContent(data)
    $('#tesseract-modal').modal('hide')
  })

  return true
}

const saveTranscription = (id, btn) => {
  const url = Routing.generate('media_transcription_save', {
    id: id
  })
  $.ajax({
    method: 'POST',
    url: url,
    data: {
      'transcription': editor.getContent()
    }
  }).done(() => {
    Toastr.info(Translator.trans('transcription_saved'))
    mainSaveBtn.classList.add('btn-secondary')
    mainSaveBtn.classList.remove('btn-info')
  })

  return true
}

const reportTranscription = (id) => {
  $('#report-modal').modal('hide')
  const url = Routing.generate('media_transcription_report', {
    id: id
  })
  $.ajax({
    method: 'POST',
    url: url,
    data: {
      'reportType': document.getElementById('report').value
    }
  }).done(() => {
    Toastr.info(Translator.trans('transcription_reported'))
  })

  return true
}

const startTutorial = () => {
  introJs().setOptions({
    'showProgress': true,
    'showBullets': false,
    'showStepNumbers': false,
    'scrollToElement': false,
    'overlayOpacity': 0.5,
    'exitOnOverlayClick': false,
    'exitOnEsc': false,
    'nextLabel': Translator.trans('tutorial_next'),
    'prevLabel': Translator.trans('tutorial_previous'),
    'doneLabel': Translator.trans('tutorial_finished'),
    'skipLabel': 'Skip'
  }).start()
}

const testFirstTranscript = () => {
  const firstTranscript = document.getElementById('firstTranscript').value
  if (firstTranscript == 1) {
    startTutorial()
    const url = Routing.generate('user_tutorial_viewed')
    $.ajax({
      method: 'POST',
      url: url
    })

    return true
  }
}

const preventLeaving = () => {
  window.onbeforeunload = function(e) {
    if (mainSaveBtn.classList.contains('btn-info')) {
      let msg = Translator.trans('leaving_confirmation')
      var e = e || window.event;
      if (e) {
        e.returnValue = msg
      }

      return msg
    }
  }
}

const displayOtherFacsimile = (btn, next) => {
  let id = btn.dataset.id

  nextBtn.disabled = true
  previousBtn.disabled = true

  const url = Routing.generate('media_transcription_next', {
    id: id,
    next: next ? 1 : 0
  })

  $.ajax({
    method: 'POST',
    url: url,
    data: {}
  }).done((data) => {
    if (data.id) {
      nextBtn.dataset.id = data.id
      previousBtn.dataset.id = data.id

      mediaViewer.show(data.type, data.url, data.options)

      Toastr.info(Translator.trans('media_change'))
      nextBtn.disabled = false
      previousBtn.disabled = false
    } else {
      if (next) {
        previousBtn.disabled = false
      } else {
        nextBtn.disabled = false
      }
      Toastr.warning(Translator.trans('no_media_available'))
    }
    if (previousBtn.dataset.id !== homeBtn.dataset.id) {
      warningDiv.classList.remove('d-none')
    } else {
      warningDiv.classList.add('d-none')
    }
  })
}

const displayOriginalFacsimile = (btn) => {
  mediaViewer.show(mediaType, mediaUrl)
  warningDiv.classList.add('d-none')
  nextBtn.dataset.id = btn.dataset.id
  previousBtn.dataset.id = btn.dataset.id
  nextBtn.disabled = false
  previousBtn.disabled = false
}
