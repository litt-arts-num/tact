$(document).ready(function () {
  $(document).on('click', '#delete-json', function () {
    formHandler.deleteJson($(this).data('project-id'))
  })

})

const formHandler = {
  deleteJson: function (projectId) {
    var url = Routing.generate('project_json_delete', {
      id: projectId
    })
    $.ajax({
      url: url,
      type: 'DELETE',
      async: true,
      success: function () {
        $('.project-json-row').empty()
      }
    })

    return false
  }
}