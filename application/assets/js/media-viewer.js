import OpenSeadragon from 'openseadragon';
import Sticky from 'sticky-js';
import {render} from './video-renderer';

export default class MediaViewer {
    constructor(options) {
        this.options = Object.assign({}, options);

        this.contentElement = document.querySelector(this.options.selectors.content);

        this._initImageViewer();
        this._initPdfViewer();
        this._initAudio();
        this._initVideo();

        this._pdfSticky = null;

        if (this.contentElement) {
            this.contentElement.style.display = 'block';
        }
    }

    show(type, url, options) {
        switch (type) {
            case 'image':
                if (this._imageViewerElement) {
                    this._hideAll()
                    this._toggleImageViewer(true)

                    this.imageViewer.open({
                        type: type,
                        url: url
                    });
                }
                
                break;

            case 'pdf':
                this._hideAll()

                const preparedUrl = url.indexOf('http://') === 0 || url.indexOf('https://') === 0 ?
                    Routing.generate('media_server_proxy_media', {
                        path: url
                    }) :
                    url;

                this._pdfViewerElement.setAttribute('src', preparedUrl);
                this._togglePdfViewer(true)

                break;

            case 'audio':
                this._hideAll()
                this._audioPlayerElement.setAttribute('src', url)
                this._toggleAudioPlayer(true)
                break;

            case 'video':
                this._hideAll()
                this._renderVideoPlayer(url, options)
                break;

            default:
                throw new Error(`Unknown media type "${type}"`);
        }

        if (typeof this.options.onShow === 'function') {
            this.options.onShow({
                type,
                url
            })
        }
    }

    onShow(callback) {
        this.options.onShow = callback;
    }

    _hideAll() {
        this._toggleImageViewer(false)
        this._togglePdfViewer(false);
        this._toggleAudioPlayer(false);
        this._hideVideoPlayer(false);

        if (this._pdfViewerElement) {
            this._pdfViewerElement.removeAttribute('src')
        }
        
        if (this._audioPlayerElement) {
            this._audioPlayerElement.removeAttribute('src')
        }
    }

    _initPdfViewer() {
        this._pdfViewerElement = document.querySelector(this.options.selectors.pdfViewer);
    }

    _initAudio() {
        this._audioPlayerElement = document.querySelector(this.options.selectors.audioPlayer);
    }

    _initVideo() {
        this._videoPlayerElement = document.querySelector(this.options.selectors.videoPlayer);
    }

    _initImageViewer() {
        this._imageViewerElement = document.querySelector('#' + this.options.selectors.imageViewerId);
        this._imageViewerButtonsElement = document.querySelector(this.options.selectors.imageViewerPanel);

        this._toggleImageViewer(false)

        if (this._imageViewerElement) {
            this.imageViewer = new OpenSeadragon.Viewer({
                id: this.options.selectors.imageViewerId,
                showNavigator: false,
                collectionMode: false,
                showRotationControl: true,
                showReferenceStrip: true,
                referenceStripScroll: 'vertical',
                prefixUrl: '',
                zoomInButton: 'osd-zoom-in',
                zoomOutButton: 'osd-zoom-out',
                rotateLeftButton: 'osd-left',
                rotateRightButton: 'osd-right',
                //tileSources: item
            })
            // disable keyboard shortcuts
            this.imageViewer.innerTracker.keyHandler = null;
        }
    }

    _toggleImageViewer(visible) {
        if (this._imageViewerElement) {
            this._imageViewerElement.style.display = visible ? 'block' : 'none';
        }
        if (this._imageViewerButtonsElement) {
            this._imageViewerButtonsElement.style.display = visible ? 'block' : 'none';
        }
    }

    _togglePdfViewer(visible) {
        if (this._pdfViewerElement) {
            this._pdfViewerElement.style.display = visible ? 'block' : 'none';

            if (!this._pdfSticky && visible) {
                this._pdfSticky = new Sticky(this.options.selectors.pdfViewer);
            }

            if (!visible) {
                if (this._pdfSticky) {
                    this._pdfSticky.destroy();
                    this._pdfSticky = null;
                }
            }
        }
    }

    _hideVideoPlayer() {
        if (this._videoPlayerElement) {
            this._videoPlayerElement.innerHTML = '';
        }
    }

    _renderVideoPlayer(url, options) {
        this._videoPlayerElement.appendChild(render(options.videoType, url, 560, 340));
    }

    _toggleAudioPlayer(visible) {
        if (this._audioPlayerElement) {
            this._audioPlayerElement.style.display = visible ? 'block' : 'none';
            if (!visible && !this._audioPlayerElement.paused) {
                this._audioPlayerElement.pause()
                this._audioPlayerElement.currentTime = 0;
            }
        }
    }
}
