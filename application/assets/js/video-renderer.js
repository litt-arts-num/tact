const renderIframeVideo = (src, width, height) => {
    const result = document.createElement('iframe');
    result.setAttribute('class', 'video-player-preview');
    result.setAttribute('width', width);
    result.setAttribute('height', height);
    result.setAttribute('src', src);
    result.setAttribute('allowfullscreen', true);
    result.setAttribute('frameborder', 0);

    return result;
}

export const renderYoutubePlayer = (videoId, width, height) => renderIframeVideo(`https://www.youtube.com/embed/${videoId}`, width, height);

export const renderVimeoPlayer = (videoId, width, height) => renderIframeVideo(`https://player.vimeo.com/video/${videoId}`, width, height);

export const renderPeertubePlayer = (videoId, width, height) => renderIframeVideo(`https://peertube.fr/videos/embed/${videoId}`, width, height);

export const renderCanalUPlayer = (videoId, width, height) => renderIframeVideo(`https://www.canal-u.tv/embed/${videoId}`, width, height);

export const renderLocalPlayer = (videoUrl, width, height) => {
    let localPlayer = document.createElement('video')

    localPlayer.setAttribute('src', videoUrl);
    localPlayer.setAttribute('controls', '');
    localPlayer.setAttribute('width', width);
    localPlayer.setAttribute('height', height);
    localPlayer.setAttribute('class', 'video-player-preview')

    return localPlayer;
}

export const render = (type, video, width = 640, height = 360) => {
    switch (type) {
        case 'youtube':
            return renderYoutubePlayer(video, width, height);
        case 'vimeo':
            return renderVimeoPlayer(video, width, height);
        case 'peertube':
            return renderPeertubePlayer(video, width, height);
        case 'canal-u':
            return renderCanalUPlayer(video, width, height);
        default:
            return renderLocalPlayer(video, width, height);
    }
}
