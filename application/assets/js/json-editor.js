import {createApp} from 'vue'
import Entrees from "./components/entrees.js"
import Tagitem from "./components/tagitem.js"
import Tagattribute from "./components/tagattribute.js"
import Tagchildren from "./components/tagchildren.js"


const VueApp = {
  mounted() {
    this.init()
  },
  data() {
    return {
      tags: [],
      count: 0,
      err: false,
      downloadurl: "",
      importedfilename: "fichier_resultat.json",
      browsefile: "",
      projectId: null,
      existing: false,
      deleteConfirm: false
      // downloadurlcss: "",
      // cssfilename: "style_resultat.css",
    }
  },
  methods: {
    async init() {
      // try to use already existing JSON for the project.
      this.projectId = document.querySelector("#vue").dataset.project
      const url = "/project_files/"+this.projectId+"/tei-schema.json"
      const options = {
        method: 'GET',
        headers: {'Content-Type': 'application/json' }
      }
      
      const response = await fetch(url, options)

      if (response.ok) {
        this.existing = true
        const data = await response.json();
        const json = JSON.stringify(data)
        const fileInput = document.querySelector('#file-selector-vue');

        // Get your file ready
        const myFileName = 'my_file.json';
        const myFile = new File([json] , myFileName);

        // Create a data transfer object. Similar to what you get from a `drop` event as `event.dataTransfer`
        const dataTransfer = new DataTransfer();

        // Add your file to the file list of the object
        dataTransfer.items.add(myFile);

        // Save the file list to a new variable
        const fileList = dataTransfer.files;

        // Set your input `files` to the file list
        fileInput.files = fileList;
        
        // trigger change event
        fileInput.dispatchEvent(new Event('change', { bubbles: true }));

      } else {
        console.log('no existing json for the project');
      }

    },
    saveToProject() {
      let jsonresult = {
        elements: this.tags
      }
      var json = JSON.stringify(jsonresult)

      const url = Routing.generate('project_save_json', {
        id: this.projectId
      })
      $.ajax({
        method: 'POST',
        url: url,
        data: {
          json: json
        }
      }).done((data) => {
        if (data.saved === true) {
          Toastr.info("Schéma associé au projet")
          this.existing = true
          this.deleteConfirm = false
        } else {
          Toastr.warning("Erreur sauvegarde")
        }
      })
    },
    deleteFromProject(confirm) {
      if(confirm) {
        const url = Routing.generate('project_json_delete', {
          id: this.projectId
        })
        $.ajax({
          method: 'DELETE',
          url: url
        }).done((data) => {
          if (data.deleted === true) {
            Toastr.info("Schéma supprimé du projet")
            this.existing = false
          } else {
            Toastr.warning("Erreur suppression")
          }
        })
      } else {
        this.deleteConfirm = false
      }
      
    },
    addTag(message) {
      if (!this.tags.some((element) => element.tag == message)) {
        this.tags.push({
            'tag': message,
            'id': this.count,
            'help': "",
            'rootDisplayed': true,
            'attributes': [],
            'children': []
            // 'bold': false,
            // 'italic': false,
            // 'underline': false,
            // 'strikethrough': false,
            // 'textsize': 'medium',
            // 'color': "black",
            // 'backgroundcolor': "white"
          }),
          this.count++
        this.message = ""
      } else {
        this.err = true
      }
    },
    removeItem(index) {
      var deleted_tag = this.tags[index].tag
      if (this.tags.length == 1) {
        this.tags = []
        this.count--
      } else if (index + 1 == this.tags.length) {
        this.tags.pop()
        this.count--
        this.tags.forEach(item => {
          if (item.id > index) {
            item.id--
          }
          item.children.splice(item.children.indexOf(deleted_tag), 1)
        })
      } else if (index < this.tags.length) {
        this.tags.splice(index, 1)
        this.count--
        this.tags.forEach(item => {
          if (item.id > index) {
            item.id--
          }
          item.children.splice(item.children.indexOf(deleted_tag), 1)
        })
      }
    },
    removeError() {
      this.err = false
    },
    reset() {
      this.tags = []
      this.count = 0
      const fileInput = document.querySelector('#file-selector-vue');
      fileInput.value = "";
    },
    save() {
      const btn = document.querySelector("#telechargement")
      
      var oldValue = btn.innerHTML
      btn.setAttribute('disabled', true);
      btn.innerHTML = 'Génération du JSON ...';
      setTimeout(function(){
          btn.innerHTML = oldValue;
          btn.removeAttribute('disabled');
      }, 2000)

      // se passer de cette phase et générer au clic sur sauvegarder localement
      let jsonresult = {
        elements: this.tags
      }
      var data = new Blob([JSON.stringify(jsonresult, null, 4)], {
        type: 'application/json'
      })


      // var prefix = "#preview "
      // var csstext = ""
      // jsonresult.elements.forEach((item) => {
        // var tag = item.tag.replace(":", '\\:')
        //   csstext += prefix + tag + " {\n\tcolor: " + item.color + ";\n"
        //   csstext += "\tbackground-color: " + item.backgroundcolor + ";\n"
        //   if (item.textsize == "large") {
        //     csstext += "\tfont-size: x-large;\n"
        //   }
        //   if (item.textsize == "small") {
        //     csstext += "\tfont-size: small;\n"
        //   }
        //   if (item.bold) {
        //     csstext += "\tfont-weight: bold;\n"
        //   }
        //   if (item.italic) {
        //     csstext += "\tfont-style: italic;\n"
        //   }
        //   if (item.underline && item.strikethrough) {
        //     csstext += "\ttext-decoration: underline line-through;\n"
        //   } else if (item.underline) {
        //     csstext += "\ttext-decoration: underline;\n"
        //   } else if (item.strikethrough) {
        //     csstext += "\ttext-decoration: line-through;\n"
        //   }
        //   csstext += "}\n\n"
      // })
      // var cssdata = new Blob([csstext], {
      //   type: 'text/plain'
      // })
      var url = window.URL.createObjectURL(data)
      btn.href = url
      
      // var urlcss = window.URL.createObjectURL(cssdata)
      // this.downloadurlcss = urlcss
    },
    fileread(file, callbackfunction) {
      var reader = new FileReader()
      reader.onload = callbackfunction
      reader.readAsText(file)
    },
    fileload(event) {
      const file = event.target.files[0]
      document.querySelector("#collapse-upload").classList.remove("show")
      this.importedfilename = file.name
      var that = this
      this.fileread(file, function(answer) {
        that.tags = JSON.parse(answer.target.result).elements
        that.count = 0
        that.tags.forEach(item => {
          if (!item.hasOwnProperty("id")) {
            item.id = that.count
          }
          if (!item.hasOwnProperty("help")) {
            item.help = ""
          }
          if (!item.hasOwnProperty("rootDisplayed")) {
            item.rootDisplayed = true
          }
          if (!item.hasOwnProperty("children")) {
            item.children = []
          }
          // if (!item.hasOwnProperty("bold")) {
          //   item.bold = false
          // }
          // if (!item.hasOwnProperty("italic")) {
          //   item.italic = false
          // }
          // if (!item.hasOwnProperty("underline")) {
          //   item.underline = false
          // }
          // if (!item.hasOwnProperty("strikethrough")) {
          //   item.strikethrough = false
          // }
          // if (!item.hasOwnProperty("textsize")) {
          //   item.textsize = "medium"
          // }
          // if (!item.hasOwnProperty("color") || item.color == "") {
          //   item.color = "black"
          // }
          // if (!item.hasOwnProperty("backgroundcolor")) {
          //   item.backgroundcolor = "white"
          // }
          that.count++
        })
      })
    }
  }
}

const appli = createApp(VueApp)

appli.component("entrees", Entrees)
appli.component("tag-item", Tagitem)
appli.component("tag-attribute", Tagattribute)
appli.component("tag-children", Tagchildren)

appli.mount('#vue')