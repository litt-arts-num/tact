export default {
  props: ['attribute'],
  emits: ['deleteattr'],
  data() {
    return {
      unfoldattr: false,
      newvaluename: "",
      err: false
    }
  },
  methods: {
    changefold() {
      this.unfoldattr = !this.unfoldattr
    },
    changetype(newtype) {
      this.attribute.type = newtype
      if (newtype == "string") {
        this.attribute.values = []
      }
    },
    changereq() {
      this.attribute.required = !this.attribute.required
    },
    newvalue() {
      if (!this.attribute.values.some((element) => element == this.newvaluename)) {
        this.attribute.values.push(this.newvaluename)
        this.newvaluename = ""
      } else {
        this.err = true
      }

    },
    delvalue(value) {
      this.attribute.values.splice(this.attribute.values.indexOf(value), 1)
    }
  },
  template: `<li class="list-group-item" id="attributes">
    {{attribute.key}}

    <button @click="changefold" class="btn btn-sm btn-secondary float-right" v-if="!unfoldattr">▼</button>
    <button @click="changefold" class="btn btn-sm btn-secondary float-right" v-if="unfoldattr">▲</button>

    <template v-if="unfoldattr">
      <div class="input-group mt-4">
        <span class="input-group-text">Nom</span>
        <input class="form-control" v-model="attribute.key" placeholder="Editer"/>
      </div>

      <div class="btn-group mt-2" role="group">
        <button @click="changereq" class="btn btn-sm btn-secondary" :class="{active: !attribute.required}">Attribut non-obligatoire</button>
        <button @click="changereq" class="btn btn-sm btn-secondary" :class="{active: attribute.required}">Attribut obligatoire</button>
      </div>

      <div class="btn-group mt-2" role="group">
        <button @click="changetype('enumerated')" class="btn btn-sm btn-secondary" :class="{active: attribute.type=='enumerated'}">
          Liste de valeurs prédefinies
        </button>
        <button @click="changetype('string')" class="btn btn-sm btn-secondary" :class="{active: attribute.type=='string'}">
          Chaîne de caractères
        </button>
      </div>

      <div class="mt-2" v-if="attribute.type=='enumerated'">
        Valeurs:
        <ul class="list-group">
          <li class="list-group-item list-group-item-secondary">
            <div>
              <input class="attr-value-input" v-model="newvaluename" placeholder="Nom nouvelle valeur"/>
              <button @click="newvalue" v-if="newvaluename!=''" class="float-end btn btn-sm btn-success">+</button>
            </div>
            <div v-if="err">Erreur: la valeur indiquée existe déjà!</div>
            <button @click="this.err=!this.err" v-if="err" class="btn btn-sm btn-secondary">OK</button>
          </li>

          <li class="list-group-item" v-for="value in attribute.values">
            {{value}}
            <button @click="delvalue(value)" class="btn btn-sm btn-danger float-end">-</button>
          </li>
        </ul>
      </div>

      <button @click="$emit('deleteattr',attribute)" class="mt-2 btn btn-sm btn-danger float-end">Effacer attribut</button>
      <br>
    </template>
  </li>
  `

}
