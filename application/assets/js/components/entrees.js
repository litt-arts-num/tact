export default {
  props: ['tags'],
  emits: ["newtag"],
  data() {
    return {
      namespaceError: false,
      tagName: "",
      htmlElements: ["a", "abbr", "address", "area", "article", "aside", "audio", "b", "base", "bdi", "bdo", "blockquote", "body", "br", "button", "canvas", "caption", "cite", "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "label", "legend", "li", "link", "main", "map", "mark", "meter", "nav", "noframes", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre", "progress", "q", "rp", "rt", "rtc", "ruby", "s", "samp", "script", "section", "select", "slot", "small", "source", "span", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "template", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"]
    }
  },
  methods: {
    checkSyntax(){
      const regex = /\s/g;
      this.tagName = this.tagName.replace(regex, '')
      this.namespaceError = false
      if (this.htmlElements.includes(this.tagName) && this.tagName.includes('')) {
        this.namespaceError = true
      }

      return true
    },
    sendinfo() {
      this.$emit('newtag', this.tagName)
      this.tagName = ""
    }
  },
  template: `
      <h5>Ajout d'un nouvel élément</h5>
      <div class="input-group">
        <input id="new-tag" v-on:keyup="checkSyntax" v-model="tagName" placeholder="Nom du nouvel élément" class="form-control" />
        <button v-if="checkSyntax() && !namespaceError && tagName!='' && !this.tags.some((element) => element.tag == tagName)" class="btn btn-success" type="button" @click="sendinfo">+</button>
      </div>
      <div v-if="namespaceError">
        Certains éléments de la TEI sont représentés par les mêmes balises que certains éléments HTML, dont <code>{{ tagName }}</code>. TinyMce, que l'on utilise comme éditeur dans TACT, peut poser problème (d'affichage ou de structure du document). Mieux vaut donc ajouter un namespace à vos élements (<code>tei:{{ tagName }}</code>)
      </div>
    `
}
