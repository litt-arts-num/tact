export default {
  props: ['item', 'tags'],
  emits: ['delete-item'],
  data() {
    return {
      unfold: false,
      newattrname: "",
      err: false,
      // stylelist: ["bold", "italic", "underline"],
      // colorlist: ["black", "silver", "gray", "white", "maroon", "red", "purple", "fuchsia", "green", "lime", "olive", "yellow", "navy", "blue", "teal", "aqua"],
    }
  },
  methods: {
    reformatTag(tagname){
      return tagname.replace(/:/i, '-')
    },
    toggleFold() {
      this.unfold = !this.unfold
    },
    addchild(child) {
      if (!(this.item.children.includes(child))) {
        this.item.children.push(child)
      }
    },
    deletechild(child) {
      if (this.item.children.includes(child)) {
        this.item.children.splice(this.item.children.indexOf(child), 1)
      }
    },
    addattr() {
      if (!this.item.attributes.some((element) => element.key == this.newattrname)) {
        this.item.attributes.push({
          'key': this.newattrname,
          'type': "enumerated",
          'required': false,
          'values': []
        })
        this.newattrname = ""
      } else {
        this.err = true
      }

    },
    deleteattr(attribute) {
      this.item.attributes.splice(this.item.attributes.indexOf(attribute), 1)
    },
    removeError() {
      this.err = false
    },
    changeRoot() {
      this.item.rootDisplayed = !this.item.rootDisplayed
    },
    switchstyleitem(item) {
      if (this.item.style.includes(item)) {
        this.item.style.splice(this.item.style.indexOf(item), 1)
      } else {
        this.item.style.push(item)
      }
    },
    addAllChildren() {
      for (const element of this.tags) {
        if (!this.item.children.includes(element.tag)) {
          this.item.children.push(element.tag)
        }
      }
    },
    deleteAllChildren() {
      this.item.children = []
    }
  },
  template: `
  <div class="col mb-2">
    <div class="card itemlist">
      <div class="card-body">
        <h5 class="card-title text-center">
          <span class="text-muted">&lt;</span>
          {{item.tag}}
          <span class="text-muted">&gt;</span>
          <button class="btn btn-sm btn-secondary float-right" @click="toggleFold" v-if="!unfold">▼</button>
          <button class="btn btn-sm btn-secondary float-right" @click="toggleFold" v-if="unfold">▲</button>
        </h5>

        <template v-if="unfold">
          <!--
          <div class="input-group mt-4">
            <span class="input-group-text">Nom</span>
            <input class="form-control" v-model="item.tag" placeholder="Nom de l'élément"/>
          </div>
          -->

          <div class="accordion mt-2" v-bind:id="'accordion'+reformatTag(item.tag)">

            <div class="card">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" :data-target='"#collapse-help"+reformatTag(item.tag)' aria-expanded="true" aria-controls="collapseOne">
                    Message d'aide
                  </button>
                </h5>
              </div>
              <div :id='"collapse-help"+reformatTag(item.tag)' class="collapse" aria-labelledby="headingOne">
                <div class="card-body">
                    <textarea class="form-control" v-model="item.help" placeholder="texte d'aide (optionnel)"/>
                </div>
              </div>
            </div>


            <div class="card">
              <div class="card-header">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="'#collapseTwo'+reformatTag(item.tag)" aria-expanded="true" aria-controls="collapseOne">
                    Attributs
                  </button>
                </h5>
              </div>
              <div :id="'collapseTwo'+reformatTag(item.tag)" class="collapse">
                <div class="card-body">
                  <div><b>Ajout d'un nouvel attribut:</b></div>
                  <div class="input-group">
                    <input class="form-control" placeholder="name of new attribute" v-model="newattrname"/>
                   <button v-if="newattrname!=''" class="btn btn-outline-secondary" type="button" id="button-addon1" @click="addattr">+</button>
                  </div>
                  <div v-if="err">Erreur: l'attribut indiqué existe déjà!</div>
                  <button @click="removeError" v-if="err">OK</button>

                  <ul class="mt-4 list-group">
                    <tag-attribute v-for="attribute in item.attributes" v-bind:attribute="attribute" @deleteattr="deleteattr"></tag-attribute>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="'#collapseThree'+reformatTag(item.tag)" aria-expanded="true">
                    Sous-éléments
                  </button>
                </h5>
              </div>
              <div :id="'collapseThree'+reformatTag(item.tag)" class="collapse">
                <div class="card-body">
                  <tag-children v-for="tag in tags" v-bind:child="tag.tag" v-bind:parent="item.tag" v-bind:childlist="item.children" @newchild="addchild" @removechild="deletechild"></tag-children>

                  <div class="text-center mt-2">
                    <div class="btn-group" role="group">
                        <button class="btn btn-sm btn-outline-secondary" @click="addAllChildren()">Tout ajouter</button>
                        <button class="btn btn-sm btn-outline-secondary" @click="deleteAllChildren()">Tout retirer</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="'#collapseFour'+reformatTag(item.tag)" aria-expanded="true">
                    Autres options
                  </button>
                </h5>
              </div>
              <div :id="'collapseFour'+reformatTag(item.tag)" class="collapse">
                <div class="card-body">
                  <div v-if="!item.rootDisplayed">Cet étiquette ne peut être utilisée à la racine</div>
                  <div v-if="item.rootDisplayed">Cet étiquette peut être utilisée à la racine</div>
                  <button class="btn btn-sm btn-outline-secondary" @click="changeRoot" v-if="!item.rootDisplayed">Autoriser utilisation à la racine</button>
                  <button class="btn btn-sm btn-outline-secondary" @click="changeRoot" v-if="item.rootDisplayed">Interdire utilisation à la racine</button>
                </div>
              </div>
            </div>

          </div>

          <button class="mt-2 float-right btn btn-sm btn-danger" @click="$emit('delete-item')" v-if="unfold">Effacer tag</button>
        </template>
      </div>
    </div>
  </div> <!-- col -->
  `

}
